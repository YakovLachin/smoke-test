package smoke_test

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/YakovLachin/smoke-test/config"
	"github.com/sirupsen/logrus"
	"time"
	"testing"
	"github.com/stretchr/testify/assert"
	"os"
)

func TestSmoke_Test_CheckMYSQLDBConnection(t *testing.T) {
	mysqlTesting := os.Getenv("MYSQL")
	if len(mysqlTesting) > 0 {
		assertMysqlWrkingISCOrrect(t)
	}
}

func assertMysqlWrkingISCOrrect(t *testing.T) {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("channel", "mysql-testing")
	logger.Info("Starting check SQL-connection")
	c, err := config.NewMysqlConfigFromEnv()
  	assert.NoError(t, err)
	db, err := sql.Open("mysql", c.String())
	assert.NoError(t, err)

	for limit := 30; limit > 0; limit-- {
		logger.Info("Trying Connection to SQL-server, please wait ...")
		err = db.Ping()
		if err != nil {
			time.Sleep(time.Second * 3)
			continue
		}
		db.Close()
		break
	}

	assert.NoError(t, err)
}