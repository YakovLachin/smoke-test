package config

import "fmt"

type MySQLConfig struct {
	Host   string
	Port   string
	User   string
	DBName string
	Pass   string

}

func NewMysqlConfigFromEnv() (*MySQLConfig, error) {
	host, err := GetConfigValue("MYSQL_HOST")
	if err != nil {
		return nil, err
	}
	port, err := GetConfigValue("MYSQL_PORT")
	if err != nil {
		return nil, err
	}
	user, err := GetConfigValue("MYSQL_USER")
	if err != nil {
		return nil, err
	}
	dbname, err := GetConfigValue("MYSQL_DBNAME")
	if err != nil {
		return nil, err
	}
	pass, err := GetConfigValue("MYSQL_PASS")
	if err != nil {
		return nil, err
	}

	return &MySQLConfig{
		Host: host,
		DBName: dbname,
		Pass: pass,
		User: user,
		Port: port,
	}, nil
}

func (c *MySQLConfig) String() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=Local", c.User, c.Pass, c.Host, c.Port, c.DBName)
}
