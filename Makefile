IMAGE=gitlab.com/yakovlachin/smoke-test

$(IMAGE):
	@docker build -t $(IMAGE) .

db:
	@docker-compose up -d

smoke_test: db
	@docker run --rm \
	--network smoketest_default \
	-e MYSQL=true \
	-e MYSQL_HOST=sql \
	-e MYSQL_PORT=3306 \
	-e MYSQL_USER=root \
	-e MYSQL_DBNAME=users \
	-e MYSQL_PASS=qwerty \
	$(IMAGE)
