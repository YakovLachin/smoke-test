### Smoke test
 Предназначен для проверки соединения с сервисами перед запуском интеграционных тестов.
 Список сервисов:
 - mysql-server

Перед любыми тестами, на любом коде можем запустить запускаем smoke-test и не думаем больше о подключении!!! =)

#### Сборка образа
```
make gitlab.com/yakovlachin/smoke-test
```
#### Пример запуска Для Mysql-server
```
	@docker run --rm \
	-e MYSQL=true \
	-e MYSQL_HOST=sql \
	-e MYSQL_PORT=3306 \
	-e MYSQL_USER=root \
	-e MYSQL_DBNAME=users \
	-e MYSQL_PASS=qwerty \
	gitlab.com/yakovlachin/smoke-test

```

