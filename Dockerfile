FROM golang:latest
WORKDIR /go/src/gitlab.com/YakovLachin/smoke-test
COPY . .
CMD ["go", "test"]
