CREATE DATABASE IF NOT EXISTS `users`
  COLLATE 'utf8_general_ci'
  DEFAULT CHARSET 'utf8';

USE users;

CREATE TABLE `users` (
  `email`      VARCHAR(255) NOT NULL UNIQUE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT 'клиент';

